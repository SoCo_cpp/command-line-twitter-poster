#-------------------------------------------------
#
# Project created by QtCreator 2014-12-22T01:32:17
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = cltwitterpost
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp


SOURCES += \
            libtwitcurl/base64.cpp \
            libtwitcurl/HMAC_SHA1.cpp \
            libtwitcurl/oauthlib.cpp \
            libtwitcurl/SHA1.cpp \
            libtwitcurl/twitcurl.cpp \
            libtwitcurl/urlencode.cpp

HEADERS += \
            libtwitcurl/base64.h \
            libtwitcurl/HMAC_SHA1.h \
            libtwitcurl/oauthlib.h \
            libtwitcurl/SHA1.h \
            libtwitcurl/twitcurl.h \
            libtwitcurl/urlencode.h \
            libtwitcurl/twitcurlurls.h


INCLUDEPATH += $$_PRO_FILE_PWD_/inclue
LIBS += -L"$$_PRO_FILE_PWD_/libs/" -lcurl
