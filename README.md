# README #

cltwitterpost - Command Line Twitter Poster

cltwitterpost was adapted from the twitcurl Google Code project: http://code.google.com/p/twitcurl/

Requirements: Qt 5 and a suitable C++ compiler.

cltwitterpost takes only the Twitter status message as a parameter. The credentials for authenticating to Twitter are required to be prepared in the file *cltwitterpost.cfg* in the same directory as the executable. Example config:

### cltwitterpost.cfg ###

```
#!

user=user@email.com
pass=MyPassWord
ckey=WERdfvai34324kjfdakjFF22ADFff
csec=e3DfaerAFoiboijadkn3453409fsdalijdasknFqewrknfadsiolh23423fa
tkey=123456789012-tikdioqrtQRFvnafs324234nfadsASDFASDfvolied
tsec=LKNGFO534ldfknjdfaslkijw4eoriiohj234oihasfdlkdsfkjalksdfj

```

See https://apps.twitter.com to obtain the appropriate OAUTH fields:

 * user - Twitter user name
 * user - Twitter password
 * ckey - Consumer Key (API Key)
 * csec - Consumer Secret (API Secret)
 * tkey - Access Token
 * tsec - Access Token Secret