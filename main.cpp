//#include <QCoreApplication>
#include <QFile>
#include <QFileInfo>
#include <QString>
#include <QStringList>
#include "libtwitcurl/twitcurl.h"

int main(int argc, char *argv[])
{
	std::string twitReplyMsg;
	std::string twitError;
	twitCurl twit;

	std::string userName;
	std::string password;
	std::string consumerKey;
	std::string consumerSecret;
	std::string tokenKey;
	std::string tokenSecret;
	std::string message;

	QString argMsg;
	for (int i = 1;i < argc;i++)
	{
		if (i != 1)
			argMsg += " ";
		argMsg += argv[i];
	}
	argMsg = argMsg.trimmed();
	if (argMsg.startsWith('"'))
		argMsg = argMsg.right(argMsg.size()-1);
	if (argMsg.endsWith('"'))
		argMsg = argMsg.left(argMsg.size()-1);

	message = argMsg.toUtf8().data();
	printf("\ncltwitterpost:: message: '%s'", message.c_str());

	QString fileName = QFileInfo(argv[0]).absolutePath() + "/cltwitterpost.cfg";
	QFile file(fileName);

	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		printf( "\ncltwitterpost:: config file required in working directory with userpass,ckey,csec,tkey,tsec. Looked for: %s\n", qPrintable(fileName));
		return 1;
	}

	while (!file.atEnd())
	{
	   QStringList part = QString(file.readLine().trimmed()).split(QChar('='));
			if (part[0] == "user") userName = part[1].toUtf8().data();
	   else if (part[0] == "pass") password = part[1].toUtf8().data();
	   else if (part[0] == "ckey") consumerKey = part[1].toUtf8().data();
	   else if (part[0] == "csec") consumerSecret = part[1].toUtf8().data();
	   else if (part[0] == "tkey") tokenKey = part[1].toUtf8().data();
	   else if (part[0] == "tsec") tokenSecret = part[1].toUtf8().data();
	}


	if (userName.empty() || password.empty() ||
			consumerKey.empty() || consumerSecret.empty() ||
			tokenKey.empty() || tokenSecret.empty())
	{
		printf( "\ncltwitterpost:: config file missing required field userpass,ckey,csec,tkey,tsec\n");
		return 1;
	}

	twit.setTwitterUsername( userName );
	twit.setTwitterPassword( password );
	twit.getOAuth().setConsumerKey( consumerKey );
	twit.getOAuth().setConsumerSecret( consumerSecret );
	twit.getOAuth().setOAuthTokenKey( tokenKey );
	twit.getOAuth().setOAuthTokenSecret( tokenSecret );

	if ( twit.accountVerifyCredGet() )
	{
		twit.getLastWebResponse( twitReplyMsg );
		twit.getLastCurlError( twitError );
		printf( "\ncltwitterpost:: twitCurl::accountVerifyCredGet web response:\n%s\n  error:%s\n", twitReplyMsg.c_str(), twitError.c_str() );
	}
	else
	{
		twit.getLastCurlError( twitReplyMsg );
		twit.getLastCurlError( twitError );
		printf( "\ncltwitterpost:: twitCurl::accountVerifyCredGet fail response:\n%s\n  error:%s\n", twitReplyMsg.c_str(), twitError.c_str() );
		return 1;
	}

	if ( twit.statusUpdate( message ) )
	{
		twit.getLastWebResponse( twitReplyMsg );
		twit.getLastCurlError( twitError );
		printf( "\ncltwitterpost:: twitCurl::statusUpdate web response:\n%s\n  error:%s\n", twitReplyMsg.c_str(), twitError.c_str() );
	}
	else
	{
		twit.getLastWebResponse( twitReplyMsg );
		twit.getLastCurlError( twitError );
		printf( "\ncltwitterpost:: twitCurl::statusUpdate fail response:\n%s\n  error:%s\n", twitReplyMsg.c_str(), twitError.c_str() );
		twit.getLastCurlError( twitError );
		return 1;
	}

	return 0;
}
